# kustool

A tool for analyzing and working with kustomize scripts

## Usage

1. Start a repl and load [`main.py`](main.py)
2. Instantiate an analyzer:
   `analyzer = KustomizeAnalyzer(Path("../openshift-varnish-infra"))`
3. Checkout the reference configuration, e.g. the `main` branch.
4. To save the reference configuration output, run `analyzer.backup_reference_state()`.
5. On the new configuration, run `analyzer.display_changes()` to see what, if any, changes were made in the output. And to spot mistakes.
