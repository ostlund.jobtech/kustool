import os
import yaml # assuming globally available
from pathlib import Path
import subprocess

def spit(dst, s):
    with open(dst, "w") as f:
        f.write(s)

def slurp(src):
    with open(src, "r") as f:
        return f.read()

def evaluate_kustomize(p):
    out = subprocess.run(["kustomize", "build", p.parent], capture_output=True)
    assert(out.returncode==0)
    return out.stdout.decode("utf-8")

def evaluate_kustomize_to_file(dst_path, src_path):
    os.makedirs(dst_path.parent, exist_ok=True)
    output = evaluate_kustomize(src_path)
    spit(dst_path, output)
    return output

def get_new_tag(source):
    if "images" in source:
        return source["images"][0]["newTag"]

def load_yaml(filename):
    return yaml.load(slurp(filename), Loader=yaml.FullLoader)

def list_deployment_targets(repo_root):
    repo_root = Path(repo_root)
    cidata = load_yaml(repo_root.joinpath(".gitlab-ci.yml"))

    overlay_root = repo_root.parent.joinpath(
        repo_root.name + "-infra", "kustomize", "overlays")
    assert(overlay_root.exists())

    def load_overlay(name):
        return load_yaml(overlay_root.joinpath(name, "kustomization.yaml"))

    overlay_map = {overlay:{"deployment": k,
                            "overlay-name": overlay}
                   for (k, v) in cidata.items()
                   if (isinstance(v, dict) and ".deploy" == v.get("extends"))
                   for var in [v.get("variables")] if var is not None
                   for overlay in [var.get("OVERLAY")] if overlay is not None}

    for overlay_name in os.listdir(overlay_root):
        data = load_overlay(overlay_name)
        if not(overlay_name in overlay_map):
            overlay_map[overlay_name] = {"overlay-name": overlay_name}
        overlay_map[overlay_name]["overlay"] = data
    
    return list(overlay_map.values())

def display_deployment_targets(root_path):
    for data in list_deployment_targets(root_path):
        ns = data["overlay"]["namespace"]
        ok = data["overlay-name"]
        depl = data.get("deployment", "-")
        print(f"* deployment: {depl}")
        print(f"  overlay:    {ok}")
        print(f"  namespace:  {ns}")

display_deployment_targets("/home/jonas/prog/openshift-varnish")

class KustomizeAnalyzer:
    """Instantiate this providing the root path to an `-infra` repo"""
    def __init__(self, root=None, cache=None):
        self._root = (root or Path(".")).resolve()
        self._cache = cache or self._root.joinpath(".kustool")

    def find_all_kustomize(self):
        stack = []
        stack.append(self._root)
        while 0 < len(stack):
            p = stack.pop()
            if p == self._cache:
                pass
            elif p.is_file():
                if p.name == "kustomization.yaml":
                    yield p
            else:
                for c in os.listdir(p):
                    stack.append(p.joinpath(c))

    def __repr__(self):
        return f"KustomizeAnalyzer({self._root})"

    def derive_path(self, middle, outname, p):
        return self._cache.joinpath(
            middle,
            p.parent.relative_to(self._root),
            outname)

    def reference_state_path(self, p):
        return self.derive_path("reference", "output.yaml", p)

    def current_state_path(self, p):
        return self.derive_path("current", "output.yaml", p)

    def backup_reference_state(self):
        """Call this method while on the `main` branch of the infra repo to save the reference state."""
        for p in self.find_all_kustomize():
            evaluate_kustomize_to_file(self.reference_state_path(p), p)

    def save_current_state(self):
        for p in find_all_kustomize():
            evaluate_kustomize_to_file(current_state_path(p), p)


    def analyze_current(self):
        dst = []
        for p in self.find_all_kustomize():
            current_path = self.current_state_path(p)
            kustomization_source = yaml.load(slurp(p), Loader=yaml.FullLoader)
            new_tag = get_new_tag(kustomization_source)
            current_output = evaluate_kustomize_to_file(current_path, p)
            ref_path = self.reference_state_path(p)
            ref_output = slurp(ref_path)
            diff = subprocess.run(["diff", str(ref_path), str(current_path)], capture_output=True)
            dst.append({"path": p,
                        "new_tag": new_tag,
                        "ksource": kustomization_source,
                        "ref_output": ref_output,
                        "current_output": current_output,
                        "identical": ref_output == current_output,
                        "diff": diff.stdout.decode("utf-8"),
                        "has_new_tag": new_tag is not None and new_tag in current_output})
        return dst


    def display_changes(self):
        """Call this method to compare against the reference state"""
        def disp_list(header, analyses):
            if 0 < len(analyses):
                print(header)
                for x in analyses:
                    p = x["path"]
                    tag = x["new_tag"]
                    print(f"* {p.relative_to(self._root)} ({tag})")
        analyses = self.analyze_current()
        n = len(analyses)
        has_new_tag = [x for x in analyses if x["has_new_tag"]]
        missing_new_tag = [x for x in analyses if not(x["has_new_tag"]) and x["new_tag"] is not None]
        unchanged = [x for x in analyses if x["identical"]]
        changed = [x for x in analyses if not(x["identical"])]
        print(f"Identical: {len(unchanged)} of {n}")
        disp_list("Has new tag:", has_new_tag)
        disp_list("Missing new tag:", missing_new_tag)
        if 0 < len(changed):
            print(f"{len(changed)} changed:")
            for x in changed:
                p = x["path"]
                d = x["diff"]
                print(f"\n  --- CHANGES from {p}:")
                print(d)

            
    

analyzer = KustomizeAnalyzer(Path("../openshift-varnish-infra"))
